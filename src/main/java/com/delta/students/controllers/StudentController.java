package com.delta.students.controllers;

import java.util.List;

import com.delta.students.models.Student;
import com.delta.students.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1")
public class StudentController {

    @Autowired
    StudentService sService;

    @GetMapping("/students")
    public ResponseEntity<List<Student>> get() {
        List<Student> students = sService.findAll();
        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }

    @PostMapping("/students")
    public ResponseEntity<Student> save(@RequestBody Student student) {
        Student newStudent = sService.save(student);
        return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
    }
    
    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable("id") Long id) {
        Student viewStudent = sService.findById(id);
        return new ResponseEntity<Student>(viewStudent, HttpStatus.OK);
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        sService.delete(id);
        return new ResponseEntity<String>("Student was deleted successfully.", HttpStatus.OK);
    }
            
} // end of class
