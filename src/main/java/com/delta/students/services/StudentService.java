package com.delta.students.services;

import java.util.List;

import com.delta.students.models.Student;

public interface StudentService {

    List<Student> findAll();

    Student save(Student student);

    Student findById(Long id);

    void delete(Long id);
    
}
